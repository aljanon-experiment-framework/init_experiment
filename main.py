#!/usr/bin/python3

import shutil
from argparse import ArgumentParser
from pathlib import Path
from typing import List

from git import Repo

from init_experiment.git import init_repo
from init_experiment.logging import setup_global_logger
from init_experiment.subrepo import Subrepo, setup_subrepos
from init_experiment.template import build_and_add_templates
from loguru import logger


def parse_args():
    parser: ArgumentParser = ArgumentParser(
        prog="init_experiment",
        description=""" Initialize a repository for an experiment. Creates the
        directory, initializes it, and adds several git subrepos useful for
        experiments. If necessary, the various Pipfiles of the subrepos will
        be locked and commited. """,
    )
    parser.add_argument("name", type=str, help="Name of the repository.")
    parser.add_argument(
        "--basepath",
        type=Path,
        default=Path.home(),
        help="""Absolute path to the parent directory of the
                        repository. Defaults to the home directory.""",
    )
    parser.add_argument("--user.name", type=str, help="Username for git config.")
    parser.add_argument("--user.email", type=str, help="Email for git config.")
    return parser.parse_args()


@logger.catch
def main():
    setup_global_logger()
    args = parse_args()
    logger.info("Logging setup complete.")
    logger.info(f"Repository name: {args.name}.")
    repo: Repo = init_repo(args)
    if repo is None:
        logger.critical("Could not initialize repo. Aborting.")
        exit(1)
    subrepo_list: List[Subrepo] = [
        Subrepo(
            "git@gitlab.inria.fr:aljanon-experiment-framework/deployment.git",
            "deployment",
            repo,
        ),
        Subrepo(
            "git@gitlab.inria.fr:aljanon-experiment-framework/ansible_experiment.git",
            "ansible",
            repo,
        ),
        Subrepo(
            "git@gitlab.inria.fr:aljanon-experiment-framework/setup_experiment.git",
            "setup",
            repo,
        ),
        Subrepo(
            "git@gitlab.inria.fr:aljanon-experiment-framework/teardown_experiment.git",
            "teardown",
            repo,
        ),
        Subrepo(
            "git@gitlab.inria.fr:aljanon-experiment-framework/image_builder.git",
            "image_builder",
            repo,
        ),
    ]
    if not build_and_add_templates(
        repo, Path(__file__).parent / "template", subrepo_list
    ):
        logger.critical("Could not create initial commit. Aborting.")
        exit(1)
    if not setup_subrepos(subrepo_list):
        logger.critical("Could not setup subrepos. Aborting.")
        exit(1)
    repo_dir: Path = Path(repo.working_tree_dir)
    root_env: Path = repo_dir / "environment.scm"
    kameleon_env: Path = repo_dir / "image_builder/kameleon-recipes/steps/data/environment.scm"
    logger.info(f"Symlinking {kameleon_env} to {root_env}")
    kameleon_env.symlink_to(root_env)
    logger.success(f"Repository initialized successfully.")


if __name__ == "__main__":
    main()
