#!usr/bin/python3

from git import Repo, GitConfigParser, InvalidGitRepositoryError, NoSuchPathError
from configparser import NoSectionError, NoOptionError
from loguru import logger
from argparse import ArgumentParser
from typing import Optional

class GitConfig:
    config_writer: GitConfigParser
    config_reader: GitConfigParser

    def __init__(self, repo: Repo) -> None:
        self.config_writer = repo.config_writer()
        self.config_reader = repo.config_reader()

    def get_value(self, section: str, option: str) -> Optional[str]:
        try:
            return self.config_reader.get_value(section, option)
        except (NoSectionError, NoOptionError):
            return None

    def set_or_ask_value(self, section: str, option: str, value: Optional[str]) -> str:
        if value is None:
            value = input(f"Please enter a value for '{section}.{option}': ")
        self.set_value(section, option, value)
        return value
        
    def set_value(self, section: str, option: str, value: str) -> None:
        self.config_writer.set_value(section, option, value)

    def write(self) -> bool:
        try:
            self.config_writer.write()
        except IOError as e:
            logger.critical(f"Could not write config to disk: {e}.")
            return False
        return True

def init_repo(args: ArgumentParser) -> Optional[Repo]:
    repo_dir: Path = args.basepath / args.name
    repo: Repo
    try:
        repo = Repo(repo_dir)
    except (InvalidGitRepositoryError, NoSuchPathError):
        repo = Repo.init(str(repo_dir), mkdir=True)
        logger.success(f"Empty git repository initialized at {repo_dir}.")
    else:
        logger.warning(f"{repo_dir} is already a git repository.")
    if not check_config(args, repo):
        return None
    return repo

def check_config(args: ArgumentParser, repo: Repo) -> bool:
    config: GitConfig = GitConfig(repo)
    username: str = check_option(args, config, "user", "name")
    email: str = check_option(args, config, "user", "email")
    logger.info(f"user.name: {username}, user.email: {email}")
    return config.write()

def check_option(args: ArgumentParser, config: GitConfig, section: str, option: str) -> str:
    args_value: Optional[str]
    try:
        args_value = getattr(args, f"{section}.{option}")
    except AttributeError:
        args_value = None
    if args_value is not None:
        config.set_value(section, option, args_value)
        return args_value
    else:
        return config.set_or_ask_value(section, option, config.get_value(section, option))
