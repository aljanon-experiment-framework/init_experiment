#!/usr/bin/python3

from datetime import datetime
from pathlib import Path
from stat import S_IXGRP, S_IXOTH, S_IXUSR
from string import Template
from typing import Dict, List

from git import Repo
from loguru import logger

from .subrepo import Subrepo

EXECUTABLE_FILES: List[str] = [
    "run",
]


def build_and_add_templates(
    repo: Repo, template_dir: Path, subrepo_list: List[Subrepo]
) -> bool:
    return (
        build_template(repo, template_dir, subrepo_list)
        and add_template(repo, template_dir)
        and commit_template(repo)
    )


def commit_template(repo: Repo) -> bool:
    try:
        repo.index.commit("Initial commit")
    except OSError as e:
        logger.error(f"Could not commit: {e}")
        return False
    return True


def add_template(repo: Repo, template_dir: Path) -> bool:
    files_to_add: List[Path] = [
        str(f.relative_to(template_dir)) for f in list_template_files(template_dir)
    ]
    try:
        repo.index.add(files_to_add)
    except OSError as e:
        logger.error(f"Could not add {files_to_add} to index: {e}")
        return False
    return True


def list_template_files(template_dir: Path) -> List[Path]:
    return list(template_dir.rglob("*"))


# TODO: catch exceptions
def build_template(repo: Repo, template_dir: Path, subrepo_list: List[Subrepo]) -> bool:
    subrepo_msg: str = "\n".join(
        [f"- [{subrepo.dirname}]({subrepo.url})" for subrepo in subrepo_list]
    )
    repo_dir: Path = Path(repo.working_tree_dir)
    template_vars: Dict[str, str] = {
        "author": repo.config_reader().get_value("user", "name"),
        "year": datetime.now().year,
        "project_name": repo_dir.name,
        "subrepos": subrepo_msg,
    }
    for filepath in list_template_files(template_dir):
        template_path: Path = template_dir / filepath
        template: Template = Template(template_path.read_text())
        contents: str = template.safe_substitute(template_vars)
        out_path: Path = repo_dir / filepath.relative_to(template_dir)
        out_path.write_text(contents)
    for filename in EXECUTABLE_FILES:
        out_path: Path = repo_dir / filename
        out_path.chmod(out_path.stat().st_mode | S_IXUSR | S_IXGRP | S_IXOTH)
    return True
