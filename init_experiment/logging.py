import sys
from dataclasses import dataclass
from pathlib import Path
from loguru import logger
from typing import Optional, Union, IO, Callable

@dataclass
class LoggerOptions(object):
    sink: Union[Path, IO[str]]
    format: str =('[<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green>] '
            '{{<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan>}} '
            '<level>{level:' '<8}</level>: {message} ')
    level: Union[str, int] = "TRACE"
    serialize: bool = False
    enqueue: bool = False
    backtrace: bool = False
    filter: Optional[Union[Callable, str]] = None

@dataclass
class TTYLoggerOptions(LoggerOptions):
    sink: IO[str] = sys.stderr
    level: str = "INFO"

def setup_global_logger() -> None:
    logger.configure(handlers=[
        vars(TTYLoggerOptions(level="TRACE")),
    ], levels=[
        {"name": "DEBUG","color": "<bold>"},
        {"name": "INFO", "color": "<bold><blue>"},
        {"name": "CRITICAL", "color": "<bold><magenta>"},
    ])
