#!/usr/bin/python3

import subprocess
from pathlib import Path
from typing import List

from git import Repo
from loguru import logger

from .pipfile import Pipfile


class Subrepo:
    url: str
    dirname: str
    subdir: Path
    repo: Repo
    repo_dir: Path
    cloned: bool

    def __init__(self, url: str, dirname: str, repo: Repo) -> None:
        self.url = url
        self.dirname = dirname
        self.repo = Repo
        self.repo_dir = Path(repo.working_tree_dir).resolve()
        self.subdir = self.repo_dir / self.dirname
        self.cloned = False

    def clone(self) -> bool:
        try:
            logger.info(f"Cloning {self.url}.")
            subprocess.run(
                ["git", "subrepo", "clone", self.url, self.dirname],
                cwd=str(self.repo_dir),
                check=True,
                capture_output=True,
                text=True,
            )
        except subprocess.CalledProcessError as e:
            logger.error(
                f"Could not clone '{self.url}' to subdir '{self.dirname}' "
                + f"for repository located at '{self.repo_dir}'. "
                + f"return value: {e.returncode} cmd: {e.cmd} "
                + f"stdout:[{e.stdout}] stderr:[{e.stderr}]"
            )
            return False
        else:
            self.cloned = True
            return True

    @property
    def pipfile(self) -> Pipfile:
        if not hasattr(self, "_pipfile"):
            pipfile_path: Path = self.subdir / "Pipfile"
            if pipfile_path.exists():
                self._pipfile = Pipfile(pipfile_path)
            else:
                self._pipfile = None
        return self._pipfile

    def lock_add_commit_pipfile(self) -> bool:
        if self.pipfile != None:
            return self.pipfile.add_and_commit_pipfile_lock()
        return True


def setup_subrepos(subrepo_list: List[Subrepo]) -> bool:
    if len(subrepo_list) == 0:
        return False
    clone_error_list: List[Subrepo] = [x for x in subrepo_list if not x.clone()]
    if len(clone_error_list) > 0:
        clone_error_msg: str = ", ".join([str(x.subdir) for x in clone_error_list])
        logger.warning(f"Could not clone: {clone_error_msg}")
    subrepo_list = [x for x in subrepo_list if x not in clone_error_list]
    if len(subrepo_list) == 0:
        return False
    lock_error_list: List[Subrepo] = [
        x for x in subrepo_list if not x.lock_add_commit_pipfile
    ]
    subrepo_list = [x for x in subrepo_list if x not in lock_error_list]
    if len(lock_error_list) > 0:
        lock_error_msg: str = ", ".join([str(x.subdir) for x in lock_error_list])
        logger.warning(f"Could not lock: {lock_error_msg}")
    return len(clone_error_list) == 0 and len(lock_error_list) == 0
