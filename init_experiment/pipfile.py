from git import Repo
from pathlib import Path
from loguru import logger

class Pipfile:
    path: Path
    repo: Repo

    def __init__(self, path: Path, repo: Repo):
        if not path.exists():
            raise ValueError(f"{path} does not exist")
        self.path = path
        self.lock_path  = path.with_suffix(".lock")
        self.repo = Repo

    def lock(self) -> bool:
        try:
            subprocess.run(["pipenv", "lock"], cwd=self.path.parent,
                           check=True, capture_output=True, text=True)
        except CalledProcessError as e:
            logger.error(f"Could not lock Pipfile in dir {self.path.parent}\n\
                         return value: {e.returncode}\ncmd: {e.cmd}\n\
                         stdout:\n{e.stdout}\nstderr:\n{e.stderr}")
            return False
        return True

    def add_lock(self) -> bool:
        try:
            repo.index.add(self.lock_path)
        except OSError as e:
            logger.error(f"Could not add {self.lock_path} to index: {e}")
            return False
        return True

    def commit_lock(self) -> bool:
        repo_dir: Path = Path(self.repo.working_tree_dir)
        relative_path: Path = self.lock_path.relative_to(repo_dir)
        try:
            repo.index.commit(f"Initial {relative_path}.")
        except OSError as e:
            logger.error(f"Could not commit: {e}")
            return False
        return True

    def add_and_commit_pipfile_lock(self) -> bool:
        return self.add_lock() and self.commit_lock()
